import sys
from config import *
sys.path.append(basepath+'\\token')
sys.path.append(basepath+'\\auth')
sys.path.append(basepath+'\\get')
sys.path.append(basepath+'\\set')
sys.path.append(basepath+'\\delete')
from rand_token import rand_token
from auth import auth
from challenge import rnd_challange
from notification import notification
from student_data import student_data
from update_data import update_data
from column_name import column_name
from analyze_data import analyze_data
from insert_column import insert_column
from insert_data import insert_data
from insert_user import insert_user
from insert_notification import insert_notification
from user_data import user_data
from delete_student import delete_student
from delete_user import delete_user
from delete_column import delete_column
from update_user import update_user
