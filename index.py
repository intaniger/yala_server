import socket, thread, signal, time, os

from entry import *
from config import *

running = True
challange = {}
session = {}

def strfind(mother,children):
    return mother.find(children) != -1
def stop():
   running = False
def handle(req_obj, address):
    while True: # exit when client send...?
        try:
            response_data = ""
            req = recv_all(req_obj)
            print "Command:"+req
            if len(req.split("\n")) == 1:
                command = req.split("\n")[0]
                token = ""
            else:
                command = req.split("\n")[0]
                token = req.split("\n")[1]
            if strfind(command.split(" ")[0], 'hello'):
                response_data += rand_token()
            elif strfind(command.split(" ")[0], 'update') :
                if strfind(command.split(" ")[1], 'student_data'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) > 0:
                            if update_data(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
                if strfind(command.split(" ")[1], 'user_data'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) == 2:
                            if update_user(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
                if strfind(command.split(" ")[1], 'column'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) > 0:
                            if insert_column(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Err"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
                if strfind(command.split(" ")[1], 'insert_data'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) > 0:
                            if insert_data(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
                if strfind(command.split(" ")[1], 'insert_user'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) == 2:
                            if insert_user(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
                if strfind(command.split(" ")[1], 'insert_notification'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) == 2:
                            if insert_notification(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
            elif strfind(command.split(" ")[0], 'get') :
                if strfind(command.split(" ")[1], 'challenge'):
                    if not token in challange:
                        challange[token] = rnd_challange()
                    response_data += challange[token]
                elif strfind(command.split(" ")[1], 'notification'):
                    if 'username' in session[token]:
                        noti = notification()
                        response_data += noti
                    else:
                        response_data += "403 Permission Denied"
                elif strfind(command.split(" ")[1], 'student_data'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        bigdata=student_data(params)
                        response_data += bigdata
                    else:
                        response_data += "403 Permission Denied"
                elif strfind(command.split(" ")[1], 'column_name'):
                    if 'username' in session[token]:
                        response_data += column_name()
                    else:
                        response_data += "403 Permission Denied"
                elif strfind(command.split(" ")[1], 'analyze_data'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        response_data += analyze_data(params)
                    else:
                        response_data += "403 Permission Denied"
                elif strfind(command.split(" ")[1], 'user_data'):
                    if 'username' in session[token]:
                        if int(session[token]['permission']) == 2:
                            response_data += user_data()
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
            elif strfind(command.split(" ")[0], 'delete') :
                if strfind(command.split(" ")[1], 'student_data'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) > 0:
                            if delete_student(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
                elif strfind(command.split(" ")[1], 'user'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) == 2:
                            if delete_user(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
                elif strfind(command.split(" ")[1], 'column'):
                    params = req.split("\n")[2]
                    if 'username' in session[token]:
                        if int(session[token]['permission']) == 2:
                            if delete_column(params) :
                                response_data += "200 OK"
                            else:
                                 response_data += "500 Internal Server Error"
                        else:
                            response_data += "403 Permission Denied"
                    else:
                        response_data += "403 Permission Denied"
            elif strfind(command.split(" ")[0], 'auth') :
                params = req.split("\n")[2]
                username = [param for param in params.split("&") if strfind(param,'username')][0].split("=")[1]
                password = [param for param in params.split("&") if strfind(param,'password')][0].split("=")[1]
                login_result = auth(username, password, challange[token])
                if(login_result):
                    session[token]={}
                    session[token]['username']=username
                    session[token]['permission']=login_result
                    response_data += login_result
                else:
                    response_data += "403 Permission Denied"
            elif strfind(command.split(" ")[0], 'logout'):
                del session[token]
                del challange[token]
                return
            print 'Response length:',str(len(response_data))
            req_obj.sendall(str(len(response_data))+chr(255)+response_data)
        except Exception as e :
            if e.args[0] == 10054 : # An existing connection was forcibly closed by the remote host
                return
            else:
                print 'Exception',e
def recv_all(csocket):
    data=''
    tmp_data=''
    while not tmp_data:
        tmp_data = csocket.recv(1024)
        data += tmp_data
    return data
def main():
    i=0
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(("", PORT))
    serversocket.listen(100)
    fp=open(basepath+"\\version","r")
    current_version = fp.readline().strip()
    fp.close()
    print '[*]Starting Server...'
    print '****************************** Phenomenoma Yala server V.'+str(current_version)+' ******************************'
    print '"Production version."'
    print '[INFO]Program Directory = '+basepath
    print '[INFO]Listening on port:',PORT
    print '*****************************************************************************************'
    signal.signal(signal.SIGINT, stop)
    while 1:
        if not running:
            return
        else:
            i+=1
            clientfd= serversocket.accept()
            print 'Thread No.', i, "\n"
            print 'New thread: ID=', thread.start_new(handle,clientfd)
main()
