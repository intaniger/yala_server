import sys, mysql.connector
sys.path.append('../')
import config
def delete_column(params):
  try:
    global execute_param
    cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
    cursur=cnx.cursor()
    cursur.execute("SELECT * FROM student_bigdata WHERE 0")
    cursur.fetchall()
    if params in cursur.column_names:
      exact_query =  "ALTER TABLE `student_bigdata` DROP COLUMN" + "`" + params + "`"
      print 'SQL query: ', exact_query
      cursur.execute(exact_query)
      cnx.commit()
    else:
      return False
    cnx.close()
  except Exception as e:
    print e
    return False
  return True
