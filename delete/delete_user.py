import sys, mysql.connector
sys.path.append('../')
import config
def delete_user(params):
  try:
    global execute_param
    execute_param = [params]
    cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
    cursur=cnx.cursor()
    exact_query =  "DELETE FROM user WHERE ID = %s"
    print 'SQL query: ', exact_query, execute_param
    cursur.execute(exact_query,execute_param)
    cnx.commit()
    cnx.close()
  except Exception as e:
    print e
    return False
  return True
