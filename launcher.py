import os, httplib, urllib, zipfile, sys, subprocess

basepath = os.environ['USERPROFILE']+"\\yala_server"

def installer():
  zipfp = zipfile.ZipFile(os.environ['TMP']+"\\tmp_server.zip")
  main_dir = [x for x in zipfp.namelist() if x.endswith('/') and x.count('/')==1][0]
  print '[*]Main dir = '+main_dir
  print '[*]Extracting to '+os.environ['USERPROFILE']
  zipfp.extractall(os.environ['USERPROFILE'])
  print '[+]Extracted.'
  print '[*]Deleting old version @'+os.environ['USERPROFILE']+"\\yala_server"
  os.system("rmdir /S /Q "+os.environ['USERPROFILE']+"\\yala_server")
  print '[+]Deleted'
  print '[*]Replace with new version...'
  os.rename(os.environ['USERPROFILE']+"\\"+main_dir,os.environ['USERPROFILE']+"\\yala_server")
  print '[+]Installation completed!!'
def check_for_update(version=0):
    print "[*]Fetching version number..."
    update_conn = httplib.HTTPSConnection("bitbucket.org")
    update_conn.request("GET","/intaniger/yala_server/raw/master/version")
    server_version = update_conn.getresponse().read().strip()
    update_conn.close()
    if server_version > version:
        print '[*]Need to update server'
        print '[*]Downloading update...'
        download_conn =urllib.URLopener()
        download_conn.retrieve("https://bitbucket.org/intaniger/yala_server/get/master.zip",os.environ['TMP']+"\\tmp_server.zip")
        print '[+]Download completed'
        print '[*]Calling installer'
        installer()
        print '[+]Running server...'
        subprocess.Popen(["python ",basepath+"\\index.py"])
        sys.exit(0)
        #subprocess.Popen(["python ",os.environ['TMP']+"\\installer.py"])
    else:
        print '[+]up-to-date'
        print '[+]Continue running server'
        subprocess.Popen(["python ",basepath+"\\index.py"])
        sys.exit(0)
fp=open(basepath+"\\version","r")
current_version = fp.readline().strip()
fp.close()
print '[*]Checking for update'
check_for_update(current_version)
