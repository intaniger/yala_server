import sys, mysql.connector, json
sys.path.append('../')
import config
execute_param = []
def IsNumber(s):
  try:
    float(s)
    return True
  except ValueError:
    return False
def make_query(params,columns):
  request_column = params.split(" ")[0].strip()
  whereCondition = " 1 "
  whereParam = []
  if params.find("WHERE") != -1 :
    conditionArr = params.split("WHERE")[1].strip().split(" ") # EQ BETWEEN 10,19
    if conditionArr[0] in columns:
      if conditionArr[1].strip() == "BETWEEN":
        whereCondition += " AND `"+conditionArr[0]+"` BETWEEN %s AND %s "
        whereParam.append(conditionArr[2].split(",")[0].strip())
        whereParam.append(conditionArr[2].split(",")[1].strip())
      elif conditionArr[1].strip() == "=":
        whereCondition += " AND `"+conditionArr[0]+"` = %s "
        whereParam.append(conditionArr[2].strip())
  if IsNumber(params.split(" ")[1].strip()):
    interval = params.split(" ")[1].strip()
  else:
    interval = "NULL"
  if request_column in columns:
    if interval != "NULL":
      execute_param.append(interval)
      for element in whereParam:
        execute_param.append(element)
      execute_param.append(interval)
      execute_param.append(interval)
      return "SELECT floor("+request_column+"/%s), COUNT(*) FROM student_bigdata WHERE "+whereCondition+" GROUP BY floor("+request_column+"/%s) ORDER BY floor("+request_column+"/%s) ASC"
    for element in whereParam:
        execute_param.append(element)
    return "SELECT "+request_column+", COUNT(*) FROM student_bigdata WHERE "+whereCondition+" GROUP BY "+request_column
def analyze_data(params):
  data=[]
  pack={}
  global execute_param
  execute_param = []
  cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
  cursur=cnx.cursor()
  cursur.execute("SELECT * FROM student_bigdata WHERE 0")
  cursur.fetchall()
  column_name = cursur.column_names
  query_syntax = make_query(params,column_name)
  print 'SQL query: ',query_syntax , execute_param
  cursur.execute(query_syntax , execute_param)
  for row in cursur:
    data.append({'group':row[0], 'amount':row[1]})
  pack['data']=data
  cnx.close()
  fp=open(config.basepath + "\\tmp.txt","w")
  json.dump(pack,fp)
  fp.close()
  fp=open(config.basepath + "\\tmp.txt","r")
  json_string=fp.read()
  fp.close()
  return json_string
