import sys, mysql.connector, json
sys.path.append('../')
import config
def user_data():
  cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
  cursur=cnx.cursor()
  cursur.execute("SELECT * FROM user WHERE 1")
  data=[]
  pack={}
  for row in cursur:
    once_row = [element for element in row]
    single_row = {}
    index = 0
    for key in cursur.column_names:
      if((str(type(once_row[index]).__name__).find('str') == -1) and (str(type(once_row[index]).__name__).find('unicode') == -1)):
        single_row[str(key)] = str(once_row[index])
      else:
        single_row[str(key)] = once_row[index]
      index+=1
    data.append(single_row)
  pack['data']=data
  cnx.close()
  fp=open(config.basepath + "\\tmp.txt","w")
  json.dump(pack,fp)
  fp.close()
  fp=open(config.basepath + "\\tmp.txt","r")
  json_string=fp.read()
  fp.close()
  return json_string
