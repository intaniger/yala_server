import sys, mysql.connector, json
sys.path.append('../')
import config
allow_compare_symbol=['=','<','>','>=','<=','like']
execute_param=[]
def getANDcondition(params, column_names):
  global execute_param
  global allow_compare_symbol
  andarray = params.split("AND ")
  andCondition=""
  if andarray[len(andarray)-1].find("OR ") != -1 :
    andarray[len(andarray)-1]=andarray[len(andarray)-1].split("OR ")[0]
  for condition in andarray:
    if(condition == ''):
      continue
    if(condition.strip().split(" ")[1] in allow_compare_symbol):
      if(condition.strip().split(" ")[0] in column_names):
        andCondition += " AND `"+(condition.strip().split(" ")[0]+"` "+condition.strip().split(" ")[1])+" %s"
        if(condition.strip().split(" ")[1] == "like"):
          execute_param.append("%"+condition.strip().split(" ")[2].replace(chr(39),'')+"%")
        else:
          execute_param.append(condition.strip().split(" ")[2].replace(chr(39),''))
  return andCondition
def student_data(params):
  exact_query="SELECT * FROM student_bigdata WHERE 1"
  data=[]
  pack={}
  global execute_param
  execute_param = []
  cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
  cursur=cnx.cursor()
  page = params.split("<PAGE>")[1]
  params = params.split("<PAGE>")[0]
  cursur.execute("SELECT * FROM student_bigdata WHERE 0")
  cursur.fetchall()
  column_name = cursur.column_names
  cursur.execute("SELECT COUNT(*) AS total_count FROM student_bigdata WHERE 1"+getANDcondition(params, column_name),tuple(execute_param))
  execute_param = []
  total_result=cursur.fetchone()[0]
  exact_query += getANDcondition(params, column_name)
  exact_query += ' LIMIT '+str(50 * int(page) - 50)+', 50'
  print 'SQL query: ',exact_query , execute_param
  cursur.execute(exact_query,tuple(execute_param))
  for row in cursur:
    once_row = [element for element in row]
    single_row = {}
    index = 0
    for key in cursur.column_names:
      if((str(type(once_row[index]).__name__).find('str') == -1) and (str(type(once_row[index]).__name__).find('unicode') == -1)):
        single_row[str(key)] = str(once_row[index])
      else:
        single_row[str(key)] = once_row[index]
      index+=1
    data.append(single_row)
  pack['data']=data
  pack['column']=[(str(column)) for column in column_name]
  pack['paging']={'result_length':total_result, 'current_page':page}
  cnx.close()
  fp=open(config.basepath + "\\tmp.txt","w")
  json.dump(pack,fp)
  fp.close()
  fp=open(config.basepath + "\\tmp.txt","r")
  json_string=fp.read()
  fp.close()
  return json_string
