import sys, mysql.connector, json
sys.path.append('../')
import config
def column_name():
  data=[]
  pack={}
  cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
  cursur=cnx.cursor()
  cursur.execute("SELECT column_name, column_type FROM information_schema.columns WHERE table_name='student_bigdata'")
  for row in cursur:
    data.append({'column_name':row[0], 'column_type':row[1]})
  pack['column']=data
  cnx.close()
  fp=open(config.basepath + "\\tmp.txt","w")
  json.dump(pack,fp)
  fp.close()
  fp=open(config.basepath + "\\tmp.txt","r")
  json_string=fp.read()
  fp.close()
  return json_string
