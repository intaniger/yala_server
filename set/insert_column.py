import sys, mysql.connector
sys.path.append('../')
import config
allow_datatype = ['Text','Double']
def get_update_query(params):
  param_arr = params.split(" ")
  if param_arr[1].strip() in allow_datatype :
    if param_arr[1].strip() == 'Double':
      return "ALTER TABLE student_bigdata ADD "+"`"+param_arr[0].strip().replace(" ","_")+"` "+param_arr[1].strip() +" DEFAULT '0'"
    return "ALTER TABLE student_bigdata ADD "+"`"+param_arr[0].strip().replace(" ","_")+"` "+param_arr[1].strip()
  return ""
def insert_column(params):
  try:
    global execute_param
    execute_param = []
    cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
    cursur=cnx.cursor()
    get_update_query(params)
    exact_query =  get_update_query(params) #"ALTER TABLE student_bigdata ADD %s %s"
    print 'SQL query: ',exact_query
    cursur.execute(exact_query)
    cnx.commit()
    cnx.close()
  except Exception as e:
    print e
    return False
  return True
