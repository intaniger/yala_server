import sys, mysql.connector
sys.path.append('../')
import config
execute_param=[]
def getWHEREcondition(params, column_names):
  global execute_param
  result_syntax = ""
  expression = params.split("WHERE")
  if len(expression) == 2:
    set_syntax = expression[0].strip()
    condition_syntax = expression[1].strip()
    if(set_syntax.split("=")[0].strip() in column_names and set_syntax.split("=")[0].strip() != "ID"):
      if(condition_syntax.split("=")[0].strip() == "ID"):
        result_syntax += " SET `"+set_syntax.split("=")[0].strip()+"` = %s WHERE ID = %s"
        execute_param.append(set_syntax.split("=")[1].strip())
        execute_param.append(condition_syntax.split("=")[1].strip())
  return result_syntax
def update_data(params):
  try:
    exact_query="UPDATE `student_bigdata` "
    global execute_param
    execute_param = []
    cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
    cursur=cnx.cursor()
    cursur.execute("SELECT * FROM `student_bigdata` WHERE 0")
    cursur.fetchall()
    column_name = cursur.column_names
    exact_query += getWHEREcondition(params, column_name)
    print 'SQL query: ',exact_query , execute_param
    cursur.execute(exact_query,tuple(execute_param))
    cnx.commit()
    cnx.close()
  except Exception as e:
    print e
    return False
  return True
