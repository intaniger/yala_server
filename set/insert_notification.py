import sys, mysql.connector
sys.path.append('../')
import config
execute_param=[]
def getINSERTsyntax(params, column_names):
  global execute_param
  head_syntax = "INSERT INTO notification (`public_date`,"
  tail_syntax = ") VALUES(CURRENT_TIMESTAMP,"
  data_list = params.split("&")
  for key_value in data_list:
    key = key_value.split("=")[0].strip()
    value = key_value.split("=")[1].strip()
    if key in column_names:
      head_syntax += "`"+key+"`, "
      tail_syntax += "%s,"
      execute_param.append(value)
  return head_syntax[:-2] + tail_syntax[:-1]+")"
def insert_notification(params):
  try:
    global execute_param
    execute_param = []
    cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
    cursur=cnx.cursor()
    exact_query = getINSERTsyntax(params,['headline','body','essence_level'])
    print 'SQL query: ',exact_query , execute_param
    cursur.execute(exact_query,tuple(execute_param))
    cnx.commit()
    cnx.close()
  except Exception as e:
    print e
    return False
  return True
