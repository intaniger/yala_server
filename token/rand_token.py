import hashlib, string, random
def rand_token():
  return hashlib.sha256(''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))).hexdigest()
