import sys, mysql.connector, hashlib
sys.path.append('../')
import config
def auth(login_username, hashed_password, challenge):
  cnx = mysql.connector.connect(user=config.db_user,password=config.db_password,host=config.db_server,database='yala_project')
  cursur=cnx.cursor()
  cursur.execute("SELECT * FROM user WHERE username=%s",(login_username,))
  single_row=cursur.fetchone()
  if single_row :
    if cursur.rowcount == 1:
      if hashlib.sha256(single_row[2]+challenge).hexdigest() == hashed_password:
        cnx.close()
        return str(single_row[3])
  cnx.close()
  return False
